import React from "react";
import { Switch, Route, Router } from "react-router-dom";
import Home from "../pages/home";
import Detalhe from "../pages/detalhe/detalhe";
import { history } from "../helpers/history";

const Routes = () => (
  <Router history={history}>
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/detalhe/:id" component={Detalhe} />
    </Switch>
  </Router>
);

export default Routes;
