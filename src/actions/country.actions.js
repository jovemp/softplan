import { countryConstants } from "../constants";
import { gql } from "@apollo/client";
import { ApolloClient, InMemoryCache } from "@apollo/client";
import { history } from "../helpers/history";

const client = new ApolloClient({
  uri: "https://countries-274616.ew.r.appspot.com",
  cache: new InMemoryCache(),
});

const LISTAGEM_PAIS = gql`
  query {
    Country {
      name
      capital
      area
      population
      topLevelDomains {
        name
      }
      flag {
        svgFile
      }
      location {
        latitude
        longitude
      }
      demonym
    }
  }
`;

export const CountryActions = {
  edit,
  getAll,
  getName,
};

function getAll() {
  return (dispatch) => {
    dispatch(request());

    client
      .query({ query: LISTAGEM_PAIS })
      .then((retorno) => dispatch(success(retorno.data.Country)))
      .catch((error) => dispatch(failure(error.toString())));
  };

  function request() {
    return { type: countryConstants.GETALL_REQUEST };
  }
  function success(countries) {
    return { type: countryConstants.GETALL_SUCCESS, countries };
  }
  function failure(error) {
    return { type: countryConstants.GETALL_FAILURE, error };
  }
}

function getName(name) {
  return (dispatch) => {
    dispatch(request());

    if (name.trim() === "") {
      return dispatch(failure("Nenhum nome informado"));
    }

    dispatch(success(name));
  };

  function request() {
    return { type: countryConstants.GETNAME_REQUEST };
  }
  function success(name) {
    return { type: countryConstants.GETNAME_SUCCESS, name };
  }
  function failure(error) {
    return { type: countryConstants.GETNAME_FAILURE, error };
  }
}

function edit(name, country) {
  return (dispatch) => {
    dispatch(request());

    if (!country.name.trim()) {
      return dispatch(failure(`Campo "Name" é obrigatório!`));
    }

    dispatch(success(name, country));
    history.replace("/");
  };

  function request() {
    return { type: countryConstants.EDIT_REQUEST };
  }
  function success(name, country) {
    return { type: countryConstants.EDIT_SUCCESS, country, name };
  }
  function failure(error) {
    return { type: countryConstants.EDIT_FAILURE, error };
  }
}
