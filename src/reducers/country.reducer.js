import { countryConstants } from "../constants";

export function country(state = { items: [] }, action) {
  switch (action.type) {
    case countryConstants.GETALL_REQUEST: {
      return {
        ...state,
        loading: true,
      };
    }
    case countryConstants.GETALL_SUCCESS: {
      const items = state.items.length > 0 ? state.items : action.countries;
      return {
        ...state,
        items,
      };
    }
    case countryConstants.GETALL_FAILURE: {
      return {
        ...state,
        error: action.error,
      };
    }
    case countryConstants.EDIT_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case countryConstants.EDIT_SUCCESS: {
      const items = state.items.map((a) =>
        a.name === action.name ? action.country : a
      );
      return {
        ...state,
        items,
      };
    }
    case countryConstants.EDIT_FAILURE:
      return {
        ...state,
        error: action.error,
      };

    case countryConstants.GETNAME_REQUEST: {
      return {
        ...state,
        loading: true,
      };
    }
    case countryConstants.GETNAME_SUCCESS: {
      return {
        ...state,
        item: state.items.find((c) => c.name === action.name),
      };
    }
    case countryConstants.GETNAME_FAILURE: {
      return {
        ...state,
        error: action.error,
      };
    }
    default:
      return state;
  }
}
