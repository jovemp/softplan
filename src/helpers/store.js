import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { alert } from '../reducers/alert.reducer';
import { country } from '../reducers/country.reducer';

export const store = createStore(
  combineReducers({alert, country}),
  applyMiddleware(
    thunkMiddleware
  )
);