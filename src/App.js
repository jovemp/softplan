import { BrowserRouter } from "react-router-dom";
import Routes from "./routes";
import { useSelector } from "react-redux";
import "./styles/global.css";

function App() {
  const alert = useSelector((state) => state.alert);

  return (
    <div>
      <nav className="navbar navbar-light bg-light">
        <span className="navbar-brand mb-0 h1">Consulta de países</span>
      </nav>
      <div className="col-md-8 offset-md-2">
        {alert?.message && (
          <div className={`alert ${alert.type}`}>{alert.message}</div>
        )}
      </div>

      <BrowserRouter>
        <Routes />
      </BrowserRouter>
    </div>
  );
}

export default App;
