import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { CountryActions } from "../../actions/country.actions";
import Card from "../../components/card";
import { useHistory } from "react-router-dom";

function Home() {
  const history = useHistory();

  const [filtro, setFiltro] = useState("");
  const countries = useSelector((state) => state.country);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(CountryActions.getAll());
  }, [dispatch]);

  const newCountries = countries.items
    ? filtro.length > 0
      ? countries.items.filter((c) =>
          c.name.toLowerCase().includes(filtro.toLowerCase())
        )
      : countries.items
    : [];

  function handleOnClickCountry(country) {
    history.push(`/detalhe/${country.name}`);
  }

  return (
    <div className="container">
      <div className="row mb-4 mt-4 ml-2 justify-content-center">
        <input
          type="text"
          value={filtro}
          onChange={(e) => setFiltro(e.target.value)}
          placeholder="Pesquisar País"
          className="col-sm-11"
          style={{ height: "42px" }}
        />
      </div>
      <div className="row">
        {newCountries.map((item) => (
          <div className="col-sm-3 mb-4" key={item.name}>
            <Card
              name={item.name}
              capital={item.capital}
              svgFile={item.flag.svgFile}
              onClick={() => handleOnClickCountry(item)}
            />
          </div>
        ))}
      </div>
    </div>
  );
}

export default Home;
