import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { CountryActions } from "../../actions/country.actions";
import { useHistory } from "react-router-dom";

function Cadastro({ match }) {
  const countries = useSelector((state) => state.country);
  const dispatch = useDispatch();
  const history = useHistory();

  const item = countries.item ? countries.item : undefined;

  const [name, setName] = useState("");
  const [capital, setCapital] = useState("");
  const [area, setArea] = useState("");
  const [population, setPopulation] = useState("");
  const [demonym, setDemonym] = useState("");
  const [topLevelDomains, setTopLevelDomains] = useState("");

  useEffect(() => {
    dispatch(CountryActions.getName(match.params.id));
  }, [match.params.id, dispatch]);

  useEffect(() => {
    setName(item?.name);
    setCapital(item?.capital);
    setArea(item?.area);
    setPopulation(item?.population);
    setDemonym(item?.demonym);
    setTopLevelDomains(
      item?.topLevelDomains && item?.topLevelDomains.length > 0
        ? item?.topLevelDomains[0].name
        : ""
    );
  }, [item]);

  function handleVoltar() {
    history.replace("/");
  }

  function handleSalvar() {
    dispatch(
      CountryActions.edit(match.params.id, {
        area,
        capital,
        demonym,
        flag: {
          svgFile: item.flag.svgFile,
        },
        name,
        population,
        topLevelDomains: [
          {
            name: topLevelDomains,
          },
        ],
      })
    );
  }

  return (
    <div className="container mt-5">
      <form>
        <div className="form-row justify-content-center">
          <img
            src={item?.flag.svgFile || ""}
            alt={item?.name}
            style={{ width: "30%" }}
            className="img-thumbnail"
          />
        </div>
        <div className="form-row">
          <div className="form-group col-md-6">
            <label htmlFor="inputName">Nome</label>
            <input
              type="text"
              className="form-control"
              id="inputName"
              value={name}
              name="nome"
              onChange={(e) => setName(e.target.value)}
            />
          </div>
          <div className="form-group col-md-6">
            <label htmlFor="inputCapital">Capital</label>
            <input
              type="text"
              className="form-control"
              id="inputCapital"
              value={capital}
              name="capital"
              onChange={(e) => setCapital(e.target.value)}
            />
          </div>
        </div>
        <div className="form-row">
          <div className="form-group col-md-3">
            <label htmlFor="inputArea">Area</label>
            <input
              type="text"
              className="form-control"
              id="inputArea"
              name="area"
              value={area}
              onChange={(e) => setArea(e.target.value)}
            />
          </div>
          <div className="form-group col-md-3">
            <label htmlFor="inputPopulacao">População</label>
            <input
              type="text"
              className="form-control"
              id="inputPopulacao"
              name="populacao"
              value={population}
              onChange={(e) => setPopulation(e.target.value)}
            />
          </div>
          <div className="form-group col-md-3">
            <label htmlFor="inputDominio">Dominio</label>
            <input
              type="text"
              className="form-control"
              id="inputDominio"
              value={demonym}
              onChange={(e) => setDemonym(e.target.value)}
            />
          </div>
          <div className="form-group col-md-3">
            <label htmlFor="inputDominioTop">Top Level Dominio</label>
            <input
              type="text"
              className="form-control"
              id="inputDominioTop"
              value={topLevelDomains}
              onChange={(e) => setTopLevelDomains(e.target.value)}
            />
          </div>
        </div>
        <div className="form-row">
          <div className="form-group col-sm-3">
            <button onClick={handleSalvar} className="btn btn-primary mt-5">
              Salvar
            </button>
            <button onClick={handleVoltar} className="btn btn-danger mt-5 ml-3">
              Cancelar
            </button>
          </div>
        </div>
      </form>
    </div>
  );
}

export default Cadastro;
