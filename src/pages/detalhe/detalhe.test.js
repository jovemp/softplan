import React from "react";
import { render, fireEvent, waitFor, screen } from "@testing-library/react";
import Detalhe from "./detalhe";
import { Provider } from "react-redux";
import { store } from "../../helpers/store";

const defaultProps = {
  match: {
    params: {
      id: "Brazil",
    },
  },
};

const renderComponent = (props = defaultProps) =>
  render(
    <Provider store={store}>
      <Detalhe {...props} />
    </Provider>
  );

describe("Detalhe component", () => {
  test("Should render the component", () => {
    const { container } = renderComponent();
    expect(container).toBeDefined();
  });

  test("Should be able change the name value", () => {
    renderComponent();

    const inputName = screen.getByRole("textbox", {
      name: /Nome/i,
    });
    fireEvent.change(inputName, { target: { value: "teste" } });

    expect(inputName).toHaveValue("teste");
  });

  test("Should be able change the capital value", () => {
    renderComponent();

    const inputCapital = screen.getByRole("textbox", {
      name: /capital/i,
    });
    fireEvent.change(inputCapital, { target: { value: "teste" } });

    expect(inputCapital).toHaveValue("teste");
  });

  test("Should be able change the area value", () => {
    renderComponent();

    const inputArea = screen.getByRole("textbox", {
      name: /area/i,
    });
    fireEvent.change(inputArea, { target: { value: "teste" } });

    expect(inputArea).toHaveValue("teste");
  });

  test("Should be able change the populacao value", () => {
    renderComponent();

    const inputPopulacao = screen.getByRole("textbox", {
      name: /População/i,
    });
    fireEvent.change(inputPopulacao, { target: { value: "teste" } });

    expect(inputPopulacao).toHaveValue("teste");
  });
});
