import React from "react";

const Card = ({ name, capital, svgFile, onClick }) => {
  return (
    <div className="card" style={{ width: "100%", height: "100%" }}>
      <img className="card-img-top" src={svgFile} alt={name} />
      <div className="card-body  align-items-bottom justify-content-end d-flex flex-column">
        <h5 className="card-title">{name}</h5>
        <p className="card-text">Capital: {capital}</p>
        <button onClick={onClick} type="button" class="btn btn-primary">
          Detalhe
        </button>
      </div>
    </div>
  );
};

export default Card;
